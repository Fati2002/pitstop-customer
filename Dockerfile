# Utilisation une balise d'image de base Docker explicite  déterministe et minimaliste
FROM node:21-alpine
# Optimisation des outils pour la produciton 
ENV NODE_ENV=production
# Définition du répértoire de travail
WORKDIR /app
# copiant les fichiers package.json et package-lock. json dans le répertoire de travait
COPY ["package.json", "package-lock.json", "./"]
# Installation des dépendences de la production 
RUN npm install --${NODE_ENV}
# Exécution des conteneurs en tant que user non privilégié (node) non pas en tant que root

COPY . .
# Définition du port sur lequel le conteneur écoute
EXPOSE 3001
# Utilisation de l'outil dumb-init en tant qu'un processus principal
CMD ["npm","start"]